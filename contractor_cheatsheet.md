# Contractor Cheatsheet

Welcome! You’re about to become a Viper Developer!

## Ownership

Your work is owned by Viper Development UG (and then eventually the client).

## Payment

You can get paid anytime for any hours that you did even if the client didn't pay yet, that's not your problem.

But don't make a thousand one euro bills...

### Payment Fees

Payment is in Euros as it's a very common currency and happens to be ours.
Any conversion fees especially to less common currencies are on you, the contractor.
As such I advise to use services like [Transferwise](https://transferwise.com) for the money transfer, that’ll give you almost no payment fees. (Effectively it's in the hand of the party with the less common currency to use the most efficient payment gateway.)

We can also use transferwise from our side usually (at least that's confirmed with India and Poland).

*Effectively this means* that you bill in euros and if you want you get your choosen currency on your account minus the transferwise fees. (If you don't want that we pay in euros to any SEPA account.)

**If you don’t use transferwise, you might LOOSE ~40EUR ON A 100EUR TRANSACTION. You are warned. All banks involved in an international transfer are unbelievably greedy.** Don’t do that except you’re in EUR and Europe, then it’s free.

## Working Times

You work whenever you want we have no desire to control you in any capacity. If you don't want to work on something anymore that's fine anytime of course but it'd be nice if you tell us :)

Be sure to read [our values](http://viperdev.io/about/) - responsiveness is important. That can also mean answering "I don't have time today but I'll help you tomorrow". Please make sure *especially if you're working with clients directly* that you can be responsive and usually answer within hours, not days.

## How your Bill Should Look Like

Ask Lasse for a freshbooks account. You can (but don’t have to) use Freshbooks.

**YOU DO NOT HAVE TO PAY FOR FRESHBOOKS NOR ENTER A TRIAL!** They’ll show ads but you can click them away. Viperdev pays for this already.

Before generating an invoice, in the top right corner, click on the `Settings` tab. This should bring you to the `Company Profile` page. Here, enter all of YOUR details (name, country, base currency: euro, address, etc,.).

Head on over to the `People` tab next, under `Clients` look for `Viper development` and click the edit button which appears on the table. Cross-check all the details of `Viper Development` are as shown below (this should be auto generated for the most part - Address, Tax Name, Tax Number, etc,.). 

Here's the data you wanna use:

```
Viper Development UG (haftungsbeschränkt)
Haakestrasse 37
Hamburg Hamburg  21075 
VAT ID DE312932940
```

To generate an invoice, simply head on over to `Time Tracking`, make sure all the required hours are `Mark as Unbilled` and click `Generate Invoice`, which appears under the `Time Tracking Tab`.

On the invoice page, before sending it, please ensure your bank account data is present under the `Notes` field. Also make sure the invoice is coming from you and not from viperdev to viperdev, freshbooks has some weird defaults there.

### Invoice Checklist

- [ ] Your name and address is on the top
- [ ] Viper Development and address and VAT ID is on the left (the payer)
- [ ] Your bank account data is present so we know where to transfer the money

## Guidelines for calculating billable hours

All the time that is spent working on the project, inclusive of chats and calls can be added to the billable hours for the day.

Take note that the time which is added to the billable hours should be fully focussed, for example: if you are working in a non-contiguous block of time, the hours actually spent working on the project should be added to the billable hours and not the intervals which aren't spent contributing to the project.

Things non-specific to the project should not be added to the billable hours, for example: learning time for general things. Learning time for project-specific things can be added to the billable hours.

## I want to learn stuff that is relevant for Viper Development!

Just ask Lasse for educational budget. You will be expected to write a document like this and you will be paid for your time spending on an online course.

Only do this if you intend to work with us for a longer period, this is an investment in you and we’re happy to do it but as a business we can’t pay you randomly for things that’s not useful for us :/

If you’ve worked with us for a longer time it’s also possible to get you to your favourite conference or so, we’ll work something out :)

## I need XXX to do my job!

Need a development license for apple or anything? It depends on the project you’re working on if it pays off for Viper Development to fund that for you but generally things up to 100eur or so are not a problem at all. Just talk to Lasse :)

Once approved you can just buy it in the name of your business, upload the expense to freshbooks and freshbooks will allow you to rebill it to me directly.

## I want to blog!

Yes! Awesome! We'll pay you for that. Please work together with Ricardo Tovar (@tovricardo on mattermost) to see what and if it makes sense. For now we'll pay 50Eur per blog post.

## I have an own idea for making something awesome!

Awesome! If you want to do it with viperdev just discuss it with all of us and we'll see if we can make a budget for it.